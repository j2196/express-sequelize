const express = require('express');
const cors = require("cors");
const db = require("./app/models/index.js");
const tutorialRoutes = require("./app/routes/tutorial.routes.js");
const productRoutes = require("./app/routes/product.routes.js");
const userRoutes = require("./app/routes/user.routes.js");

const app = express();

const corsOptions = {
    origin: "http://localhost:8081"
};

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({extended: true}));

db.sequelize.sync()
    .then(() => console.log("Synced db"))
    .catch((error) => console.log(`Failed to sync Db - ${error}`))

//Home route    
app.get("/", (req, res) => {
    res.json("Hello World")
});

//Routes
app.use("/api/tutorials", tutorialRoutes);
app.use("/api/products", productRoutes);
app.use("/api/users", userRoutes);


const PORT = process.env.PORT || 8080;
app.listen(PORT, () => console.log(`server running on port ${PORT}`));