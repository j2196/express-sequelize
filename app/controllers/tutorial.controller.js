const db = require("../models/index.js");

const Tutorial = db.Tutorial;
const Op = db.Sequelize.Op; //Op defines operators in SQL

//Create and save new tutorial
const create = (req, res) => {
  //Validate the request
  if (!req.body.title) {
    res.status(400).json({ message: "Content cannot be empty!" });
    return;
  }

  //create tutorial
  const tutorial = {
    title: req.body.title,
    description: req.body.description,
    published: req.body.published ? req.body.published : false,
  };

  //Save Tutorial in db
  Tutorial.create(tutorial)
    .then((data) => res.status(201).send(data))
    .catch((error) =>
      res
        .status(500)
        .json({
          message: error.message || "Error Occured while creating Tutorial",
        })
    );
};

//Retrieve All Tutorials from Db
const findAll = (req, res) => {
  const title = req.query.title; //To get query string from the request;
  const condition = title
    ? {
        title: {
          [Op.like]: title,
        },
      }
    : null;

  //Tutorial.findAll(
  //     {
  //         where: {
  //             {
  //          title: {[Op.like] : "%title}"
  // }
  //         }
  //     }
  // ) // SELECT * FROM tutorials WHERE title LIKE "xyz"
  // {
  //     where: {
  //       firstName: {
  //         [Op.like]: "Nathan",
  //       },
  //     },
  //   }

  Tutorial.findAll({ where: condition })
    .then((data) => {
      res.send(data);
    })
    .catch((error) =>
      res
        .status(500)
        .json({
          message:
            error.message || "Error Occured while retrieving the tutorials",
        })
    );
};

//Find a single Tutorial with an Id
const findOne = (req, res) => {
  const id = req.params.id;
  Tutorial.findByPk(id)
    .then((data) => {
      if (data) {
        res.status(200).send(data);
      } else {
        res
          .status(404)
          .json({ message: `Can't find tutorial with id - ${id}` });
      }
    })
    .catch((error) =>
      res
        .status(500)
        .json({
          message:
            error.message ||
            `Error occured while recovering tutorial with id - ${id}`,
        })
    );
};

//Update the Tutorial by Id
const update = (req, res) => {
  const id = req.body.id;

  Tutorial.update(req.body, {
    where: {
      id: id,
    },
  })
    .then((num) => {
      if (num == 1) {
        res.status(201).send("Tutorial updated successfully");
      } else {
        res
          .status(404)
          .json({
            message: `Cannot update tutorial with id - ${id}. Maybe tutorial was not found or req.body is empty`,
          });
      }
    })
    .catch((error) =>
      res.status(500).json({
        message:
          error.message ||
          `Error occured while updating tutorial with id - ${id}`,
      })
    );
};

//Delete a tutorial with specific id
const deleteOne = (req, res) => {
  const id = req.params.id;

  Tutorial.destroy({
    where: {
      id: id,
    },
  })
    .then((num) => {
      if (num == 1) {
        res.status(200).send("Tutorial deleted successfully");
      } else {
        res
          .status(404)
          .json({
            message: `Cannot delete Tutorial with id - ${id}. Maybe Tutorial not found or params are empty`,
          });
      }
    })
    .catch((error) =>
      res.status(500).json({
        message:
          error.message ||
          `Error occured while deleting Tutorial with id - ${id}`,
      })
    );
};

//Delete All Tutorials
const deleteAll = (req, res) => {
  Tutorial.destroy({
    where: {},
    truncate: true,
  })
    .then(() =>
      res.status(200).send({ message: `Tutorials deleted successfully` })
    )
    .catch((error) =>
      res
        .status(500)
        .json({
          message: error.message || "Error Occured while deleting tutorials",
        })
    );
};

//Find all published tutorials
const findAllPublished = (req, res) => {
  Tutorial.findAll({
    where: {
      published: true,
    },
  })
    .then((data) => res.status(200).json(data))
    .catch((error) =>
      res
        .status(500)
        .json({
          message:
            error.message ||
            "Error occured while retriving published Tutorials",
        })
    );
};

module.exports = {
    create, findAll, findOne, update, deleteOne, deleteAll, findAllPublished
}