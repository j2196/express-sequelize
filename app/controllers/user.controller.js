const db = require("../models/index.js");
const jwt = require("jsonwebtoken");
const env = require("../config/env.config.js");

const User = db.User;
const Op = db.Sequelize.Op;

//verifyToken
const verifyToken = (req, res, next) => {
  const authHeader = req.headers.authorization;

  if (authHeader) {
    const token = authHeader.split(" ")[1];

    jwt.verify(token, env.accessTokenSecret, (error, payload) => {
      if (error) {
        res.status(403).json({ message: error.message || "Invalid Token" });
      } else {
        req.user = payload;
      }

      next();
    });
  } else {
    return res.status(401).json({ message: "Authentication failure" });
  }
};

//get all users
const findAll = async (req, res) => {
  const user = req.user;

  if (!user.isAdmin) {
    res.status(403).json({ message: "Request failure, contact admin" });
    return;
  }

  try {
    const users = await User.findAll({ where: {} });
    res.status(200).json(users);
  } catch (error) {
    res
      .status(500)
      .json({
        message: error.message || "Error occured while retrieving all users",
      });
  }
};

//delete all users
const deleteAll = async (req, res) => {
  const user = req.user;

  if (!user.isAdmin) {
    res.status(403).json({ message: "Request failure, contact admin" });
    return;
  }

  try {
    const totalRecords = await User.destroy({ where: {}, truncate: true });
    res.status(200).json({ message: "All users deleted successfully" });
  } catch (error) {
    res
      .status(500)
      .json({
        message: error.message || "Error occured while deleting all users",
      });
  }
};


module.exports = {
    verifyToken, findAll, deleteAll
}