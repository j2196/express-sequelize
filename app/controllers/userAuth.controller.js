const db = require("../models/index.js");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const env = require("../config/env.config.js");

const salt = bcrypt.genSaltSync(10);
const User = db.User;
const Op = db.Sequelize.Op;

//register user and save in db
const register = (req, res) => {
  const { firstName, lastName, email, password, isAdmin } = req.body;

  if (!firstName || !lastName || !email || !password) {
    res.status(400).json({ message: "Fields cannot be empty" });
    return;
  }
  //demo 
  const hashPassword = bcrypt.hashSync(password, salt);

  const user = {
    firstName,
    lastName,
    email,
    isAdmin,
    password: hashPassword,
  };

  User.create(user)
    .then((userObj) =>
      {
        res
        .status(201)
        .json({ message: "user created successfully", user: userObj })}
    )
    .catch((err) =>
      res
        .status(500)
        .json({
          message: err.message || "Error Occured while registering user",
        })
    );
};

//user Login
const login = async (req, res) => {
  const { email, password } = req.body;

  if (!email || !password) {
    res.status(400).json({ message: "email or password invalid" });
    return;
  }

  try {
    const user = await User.findOne({ where: { email: email } });

    if (!user) {
      res.status(400).json({ message: "email or password invalid" });
      return;
    }

    //if user exists compare password with db hash password;
    const isPasswordMatched = bcrypt.compareSync(password, user.password);

    if (!isPasswordMatched) {
      res.status(400).json({ message: "Invalid email or password" });
      return;
    }

    const accessToken = jwt.sign(
      { email: user.email, firstname: user.firstname, isAdmin: user.isAdmin },
      env.accessTokenSecret,
      {
        expiresIn: env.accessTokenExpiry,
      }
    );

    res
      .status(200)
      .json({
        message: "login success",
        firstname: user.firstname,
        accessToken,
      });
  } catch (error) {
    res
      .status(500)
      .json({ message: error.message || "Error occured while logging user" });
  }
};


module.exports = {
    register, login
};