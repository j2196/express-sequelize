const db = require("../models/index.js");

const Product = db.Product;
const Op = db.Sequelize.Op;

//create and save new Product
const create = (req, res) => {
  const {
    name,
    brand,
    category,
    price,
    ratingValue,
    ratingCount,
  } = req.body;
  if (!name || !brand || !category || !price) {
    res.status(400).json({
      message: `Data Fields cannot be empty - name*, description*, brand*, category*, price*`,
    });
    return;
  }

  const product = {
    name,
    brand,
    category,
    price,
    ratingValue,
    ratingCount
  };

  Product.create(product)
    .then((data) => res.status(201).send(data))
    .catch((error) =>
      res
        .status(500)
        .json({ message: error || "Error Occured while creating product" })
    );
};

//get all products
const findAll = (req, res) => {
  const name = req.query.name;
  const condition = name
    ? {
        name: {
          [Op.like]: name,
        },
      }
    : null;
  Product.findAll({ where: condition })
    .then((data) => res.status(200).send(data))
    .catch((error) =>
      res
        .status(500)
        .json({
          message:
            error.message || "Error Occured while fetching all the products",
        })
    );
};

//find single product with specific id
const findOne = (req, res) => {
  const id = req.params.id;

  Product.findByPk(id)
    .then((data) => {
      if (data) {
        res.status(200).send(data);
      } else {
        res
          .status(400)
          .json({ message: `Cannot find Product with id - ${id}` });
      }
    })
    .catch((error) =>
      res
        .status(500)
        .json({
          message:
            error.message ||
            `Error Occurred while fetching product with id - ${id}`,
        })
    );
};

//Update Product by id
const update = (req, res) => {
  const id = req.body.id;

  Product.update(req.body, {
    where: {
      id: id,
    },
  })
    .then((num) => {
      if (num == 1) {
        res.status(201).send(`Product having id - ${id} Updated Successfully`);
      } else {
        res
          .status(400)
          .json({
            message: `Cannot update Product with id -${id}. Maybe product not found or req.body is empty`,
          });
      }
    })
    .catch((error) =>
      res
        .status(500)
        .json({
          message:
            error.message ||
            `Error occurred while updating product with id - ${id}`,
        })
    );
};

//Delete Product with specific Id
const deleteOne = (req, res) => {
  const id = req.params.id;

  Product.destroy({
    where: {
      id: id,
    },
  })
    .then((num) => {
      if (num == 1) {
        res.status(200).json({ message: "Product deleted successfully" });
      } else {
        res
          .status(400)
          .json({
            message: `Cannot delete product with id - ${id}. Maybe Product not found or params are missing`,
          });
      }
    })
    .catch((error) =>
      res
        .status(500)
        .json({
          message:
            error.message ||
            `Error Occured while deleting Product with id - ${id}`,
        })
    );
};

//delete all products
const deleteAll = (req, res) => {
  Product.destroy({
    where: {},
    truncate: true,
  })
    .then(() =>
      res.status(200).json({ message: "All Products deleted successfully" })
    )
    .catch((error) =>
      res
        .staus(500)
        .json({
          message: error.message || "Error Occured while deleting all products",
        })
    );
};

module.exports = {
    create, findAll, findOne, update, deleteOne, deleteAll
}