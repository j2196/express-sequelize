'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
   return await queryInterface.bulkInsert("Products", [
    {
      name: "Aashirwaad Atta",
      brand: "ITC",
      category: "Grocery",
      ratingValue: 4.2,
      ratingCount: 560
    },
    {
      name: "Aashirwaad Noodles",
      brand: "ITC",
      category: "Grocery",
      ratingValue: 3.9,
      ratingCount: 1200
    },
    {
      name: "Aashirwaad Macroni",
      brand: "ITC",
      category: "Grocery",
      ratingValue: 4.6,
      ratingCount: 860
    },
    {
      name: "Aashirwaad Vermicelli",
      brand: "ITC",
      category: "Grocery",
      ratingValue: 4.7,
      ratingCount: 590
    },
    {
      name: "Aashirwaad Chilli Powder",
      brand: "ITC",
      category: "Grocery",
      ratingValue: 4.7,
      ratingCount: 8600
    },
    {
      name: "Aashirwaad Turmeric Powder",
      brand: "ITC",
      category: "Grocery",
      ratingValue: 4.5,
      ratingCount: 6700
    },
    {
      name: "Aashirwaad Salt",
      brand: "ITC",
      category: "Grocery",
      ratingValue: 4.9,
      ratingCount: 9049
    }
   ])
  },
  down: async (queryInterface, Sequelize) => {
    return await queryInterface.bulkDelete("Products", null, {})
  }
};
