const { login, register } = require("../controllers/userAuth.controller.js");
const { verifyToken, findAll, deleteAll } = require("../controllers/user.controller.js");
const router = require("express").Router();


//auth routes
router.post("/register", register);
router.post("/login", login);

//user routes
router.get("/", verifyToken, findAll);
router.delete("/", verifyToken, deleteAll);

module.exports = router;

