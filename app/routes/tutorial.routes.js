const { create, findAll, findOne, update, deleteOne, deleteAll, findAllPublished } = require("../controllers/tutorial.controller.js");
const router = require("express").Router();

//base api routes - create, get, update, delete
router.route("/").post(create).get(findAll).put(update).delete(deleteAll);

//get publised tutorials
router.get("/published", findAllPublished);

//base Api with specific id - get, delete
router.route("/:id").get(findOne).delete(deleteOne);

module.exports =  router;