const { create, findAll, findOne, update, deleteOne, deleteAll } = require("../controllers/product.controller.js");
const router = require("express").Router();

//base api routes - create, get, update, delete
router.route("/").post(create).get(findAll).put(update).delete(deleteAll);

//base Api routes with specific ids - get, delete
router.route("/:id").get(findOne).delete(deleteOne);

module.exports = router;



