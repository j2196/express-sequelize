const env = require("./env.config.js");
const mysql2 = require("mysql2");

module.exports = {
  "development": {
    "username": env.user,
    "password": env.password,
    "database": env.database,
    "host": env.host,
    "port": env.port,
    "dialect": "mysql",
    "dialectModule": mysql2,
     "ssl": {
      minVersion: 'TLSv1.2',
      rejectUnauthorized: true  
    },
    "dialectOptions":{
      "ssl":{
         "require":true
      }
   }
  },
  "test": {
    "username": env.user,
    "password": env.password,
    "database": env.database,
    "host": env.host,
    "port": env.port,
    "dialect": "mysql",
    "dialectModule": mysql2,
     "ssl": {
      minVersion: 'TLSv1.2',
      rejectUnauthorized: true  
    },
    "dialectOptions":{
      "ssl":{
         "require":true
      }
   }

  },
  "production": {
    "username": env.user,
    "password": env.password,
    "database": env.database,
    "host": env.host,
    "port": env.port,
    "dialect": "mysql",
    "dialectModule": mysql2,
     "ssl": {
      minVersion: 'TLSv1.2',
      rejectUnauthorized: true  
    },
    "dialectOptions":{
      "ssl":{
         "require":true
      }
   }

  }
}
