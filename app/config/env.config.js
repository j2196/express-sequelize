const dotenv = require("dotenv");
dotenv.config();

const env = {
  host: process.env.CLOUD_DB_HOST,
  user: process.env.CLOUD_DB_USER,
  password: process.env.CLOUD_DB_PASSWORD,
  port: process.env.CLOUD_DB_PORT,
  database: process.env.CLOUD_DB,
  accessTokenSecret: process.env.ACCESS_TOKEN_SECRET,
  refreshTokenSecret: process.env.REFRESH_TOKEN_SECRET,
  accessTokenExpiry: process.env.ACCESS_TOKEN_EXPIRY,
  refreshTokenExpiry: process.env.REFRESH_TOKEN_EXPIRY
};

module.exports = env;